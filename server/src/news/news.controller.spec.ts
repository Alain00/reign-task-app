import { ScheduleModule } from '@nestjs/schedule';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { connection, Model } from 'mongoose';
import { New, NewsSchema, NewDocument } from './news.schema';
import {
  Request,
  RequestDocument,
  RequestSchema,
} from '../request/request.schema';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import axios from 'axios';
import dotenv from 'dotenv';
dotenv.config({
  path: __dirname + '/../../.env.test',
});

const { MONGO_URI, MONGO_URI_CI, NODE_ENV, MONGO_USER, MONGO_PASS } = process.env;
console.log(MONGO_URI);

const initialNews = [
  {
    created_at: '2021-05-27T14:02:26.000Z',
    title: 'Node.js Turns 12',
    url: 'test url',
    story_title: 'test story title',
    story_text: 'test story text',
    story_url: 'test story url',
    author: 'Alain',
    objectID: '1234',
    randomProp: 12387,
  },
  {
    created_at: '2021-05-27T12:52:40.000Z',
    title: null,
    url: 'test url',
    story_title: 'Beads: The next generation computer language and toolchain',
    story_text: 'test story text',
    story_url: 'test story url',
    author: 's3er',
    objectID: '1235',
  },
];

jest.mock('axios', () => ({
  get: () => {
    return {
      status: 200,
      data: { hits: initialNews },
    };
  },
}));
describe('NewsController', () => {
  let app: TestingModule;
  let newsController: NewsController;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(NODE_ENV == 'ci' ? MONGO_URI_CI : MONGO_URI, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          user: 'reign',
          pass: 'reign',
        }),
        MongooseModule.forFeature([{ name: New.name, schema: NewsSchema }]),
        MongooseModule.forFeature([
          { name: Request.name, schema: RequestSchema },
        ]),
        ScheduleModule.forRoot(),
      ],
      controllers: [NewsController],
      providers: [
        NewsService,
        // {
        // 	provide: getModelToken(New.name),
        // 	useValue: mockNewsModel
        // },
        // {
        // 	provide: getModelToken(Request.name),
        // 	useValue: mockRequestModel
        // },
      ],
    }).compile();

    newsController = app.get<NewsController>(NewsController);

    const newsModel = app.get<Model<New>>(getModelToken(New.name));
    const requestModel = app.get<Model<Request>>(getModelToken(Request.name));
    await newsModel.deleteMany({});
    await requestModel.deleteMany({});
    // newsModel.create(initialNews)
  });

  describe("root", () => {
      it(`should return ${initialNews.length} news`, async () => {
        expect(await newsController.getAll()).toHaveLength(initialNews.length);
      });
    
      it('should delete the second one', async () => {
        expect(await newsController.deleteOne(initialNews[1].objectID)).toBe(
          true,
        );
      });
    
      it(`should return ${initialNews.length - 1} news`, async () => {
        expect(await newsController.getAll()).toHaveLength(
          initialNews.length - 1,
        );
      });
  })
  

  afterAll(() => {
    app.close();
  });
});
