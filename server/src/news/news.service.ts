import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { New, NewDocument } from './news.schema';
import { Request, RequestDocument } from '../request/request.schema';
import axios from 'axios';
import moment from 'moment';
import { Cron, SchedulerRegistry } from '@nestjs/schedule';
import { CronJob } from 'cron';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel(New.name) private newModel: Model<NewDocument>,
    @InjectModel(Request.name) private requestModel: Model<RequestDocument>,
    private schedulerRegistry: SchedulerRegistry
  ) {
    this.setUpCron();
  }

  async setUpCron(): Promise<void> {
    if (["test", "ci"].includes(process.env.NODE_ENV)) return;
    const date: Date = await this.checkAndFetchNews()
    if (!this.schedulerRegistry.doesExists('cron', "getNews")){
      const m = moment(date).minutes();

      const job = new CronJob(`0 ${m} * * * *`, (this.checkAndFetchNews).bind(this));
      this.schedulerRegistry.addCronJob("getNews", job);
      job.start();
    }
  }

  async findAll(): Promise<New[]> {
    await this.checkAndFetchNews();
    return this.newModel
      .find({
        deleted: {
          $ne: true,
        },
      })
      .sort([['created_at', 'desc']])
      .exec();
  }

  async deleteOne(id: string): Promise<boolean> {
    const n = await this.newModel.findOne({
      objectID: id,
      deleted: { $ne: true },
    });
    if (!n) return false;

    n.deleted = true;
    await n.save();
    return true;
  }

  async checkAndFetchNews(): Promise<Date> {
    try {
      let lastRequest = await this.requestModel.findOne({});

      if (
        lastRequest &&
        moment(new Date()).diff(lastRequest.date, 'hours') < 1
      ) {
        console.log("cron job in " + moment(moment(lastRequest.date).add(1, 'hour')).diff(new Date(), 'minutes') + " minutes")
        return lastRequest.date;
      }
      console.log("cron job executed at " + moment().format("HH:mm:ss"))
      const res = await axios.get(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      );

      if (!lastRequest) lastRequest = new this.requestModel();
      lastRequest.date = new Date();
      await lastRequest.save();

      if (res.status != 200) return lastRequest.date;

      console.log(res.data.hits.length + ' new retreived');
      for (const hit of res.data.hits) {
        if (
          !(await this.newModel.exists({
            objectID: hit.objectID,
          }))
        ) {
          await this.newModel.create({
            ...hit,
          });
        } else {
          await this.newModel.updateOne(
            {
              objectID: hit.objectID,
            },
            {
              ...hit,
            },
          );
        }
      }
      return lastRequest.date;
    } catch (e) {
      console.error('cannot retrieve news');
      console.error(e);
    }
  }
}
