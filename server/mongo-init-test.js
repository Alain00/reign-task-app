var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://reign:reign@mongo:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("reign-test");
  dbo.addUser("reign", "reign", () => {
    var init = { init: true };
    dbo.collection("init").insertOne(init, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  })
});