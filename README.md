# Regin task
# Running the app
## Development 
    npm run start:dev
> This will start a docker container for the web client, another for the server-api, and another for the mongo database. Share volumes and observe the changes

## Production
    npm run start
> Will start a production build and then run it

## Test Server
    npm run test:db
> run test database at localhot:27019

    cd server
    npm run test
## Test Client
    cd client
    npm run test