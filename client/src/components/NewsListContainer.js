import React, { useEffect, useState } from 'react'
import axios from 'axios'
import NewsList from './NewsList';

export default () => {
    const [news, setNews] = useState([]);
    useEffect(()=>{
        fetchNews();
    }, [])

    const fetchNews = () => {
        axios.get("/news").then((res) => {
            setNews(res.data)
            console.log(res)
        }).catch(e=>{
            console.error(e)
        })
    }

    const handleDelete = (id) => {
        axios.delete(`/news/${id}`).then(res => {
            const temp = [...news];
            const index = temp.findIndex(n => n.objectID == id);
            if (index >= 0){
                temp.splice(index, 1)
                setNews(temp);
            }
        }).catch(e => {
            console.error(e)
        })
    }

    return (
        <NewsList news={news} onDelete={handleDelete} />
    )
}