import React from 'react'
import moment from 'moment'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default ({
    onDelete,
    news
}) => {
    return (
        <ul className="list">
            {news.map(item => (
                <li className="list-item" key={item._id}>
                    <div>
                        <span>{item.story_title || item.title}</span>
                        <span className="author"> - {item.author} - </span>
                    </div>
                    <div>
                        <span>
                            {moment(item.created_at).calendar({
                                lastDay: '[Yesterday]',
                                sameElse: 'MMM DD',
                                sameDay: 'hh:mm a'
                            })}
                        </span>
                        <a href="#" onClick={() => onDelete(item.objectID)}>
                            <FontAwesomeIcon icon="trash" />
                        </a>
                    </div>
                </li>
            ))}
        </ul>
    )
}