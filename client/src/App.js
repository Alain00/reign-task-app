import './App.scss';
import NewsListContainer from './components/NewsListContainer';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

library.add(faTrash)

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="font-size-title m-0">
          HN Feed
        </h1>
        <span className="font-size-subtitle">
          We {"<"}3 hacker news!
        </span>
      </header>
      <section className="container">
        <NewsListContainer />
      </section>
    </div>
  );
}

export default App;
