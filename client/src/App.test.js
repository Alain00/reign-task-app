import { act, cleanup, fireEvent, render, screen } from '@testing-library/react';
import App from './App';
import axios from 'axios'
import NewsListContainer from './components/NewsListContainer';

const initialNews = [
  {
    _id: "ajsdhu327834y2udjf",
    created_at: '2021-05-27T12:52:40.000Z',
    title: null,
    url: 'test url',
    story_title: 'Beads: The next generation computer language and toolchain',
    story_text: 'test story text',
    story_url: 'test story url',
    author: 's3er',
    objectID: '1235',
  }
]

jest.mock('axios', () => ({
  get: async () => {
    return Promise.resolve({
      status: 200,
      data: initialNews
    })
  },
  delete: async () => {
    return Promise.resolve({
      status: 200,
      data: {}
    })
  }
}))

afterEach(() => {
  cleanup();
})

test('renders title', async () => {
  await act(async ()=>{
    render(<App />);
  })
  const titleElement = screen.getByText(/HN Feed/i);
  expect(titleElement).toBeInTheDocument();
});


test('render the item', async () => {
  let listElement;
  await act(async () => {
    listElement = render(<NewsListContainer />);
  })
  const title = listElement.getByText(new RegExp(initialNews[0].story_title));
  expect(title).toBeInTheDocument();
})

test('delete the item', async () => {
  let listElement
  await act(async () => {
    listElement = render(<NewsListContainer />);
    
  })
  const deleteLink = listElement.container.querySelector('a');
  expect(deleteLink).toBeInTheDocument();
  await act(async () => {
    fireEvent.click(deleteLink);
  })
  expect(deleteLink).not.toBeInTheDocument();
})
